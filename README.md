# Block News Corp

[![pipeline status](https://gitlab.com/unixben/blocknewscorp/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/unixben/blocknewscorp/-/commits/main)

Block list of News Corporation properties.

I will happily take a pull request for adds, moves or changes.

## Which list should I use?

* If you use AdGuard, you should use [hosts_adguard.txt](hosts_adguard.txt).
* If you use PiHole, you should use [hosts_127.0.0.1.txt](hosts_127.0.0.1.txt).
* If you use uBlock Origin, you should use [hosts_adguard.txt](hosts_adguard.txt).
    * Source: https://twitter.com/Zulutron/status/1349545524920254465
